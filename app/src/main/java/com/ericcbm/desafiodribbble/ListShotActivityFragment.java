package com.ericcbm.desafiodribbble;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A placeholder fragment containing a simple view.
 */
@EFragment(R.layout.fragment_list_shot)
public class ListShotActivityFragment extends Fragment {

    @StringRes
    String connectivityErrorListShots;

    @ViewById
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;
    @ViewById
    RecyclerView recyclerView;
    @ViewById
    ProgressBar progressBar;
    @ViewById
    TextView textViewMessage;

    private ArrayList<Shot> shots;
    private RecyclerView.Adapter adapter;

    @AfterViews
    void calledAfterViewInjection() {
        shots = new ArrayList<>();
        adapter = new ShotAdapter(getActivity(), shots);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadShots(current_page + 1);
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Shot shot = shots.get(position);
                Gson gson = new Gson();
                String shotJson = gson.toJson(shot, Shot.class);
                DetailShotActivity_.intent(getActivity()).shotJson(shotJson).start();
            }
        }));

        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                shots.clear();
                adapter.notifyDataSetChanged();
                loadShots(1);
            }
        });
        multiSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.recyclerView, R.id.textViewMessage);

        showProgress(true);
        showMessage(false, "");
        loadShots(1);
    }

    @Background
    void loadShots(final int page) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.dribbble.com/")
                .build();

        DribbbleRestClient service = restAdapter.create(DribbbleRestClient.class);

        service.getListShotsPage(page, new Callback<ListShotsPage>() {
            @Override
            public void success(ListShotsPage listShotsPage, Response response) {
                showShots(listShotsPage.getShots());
                stopRefresh();
                Gson gson = new Gson();
                ListShotsPageBD listShotsPageBD = new ListShotsPageBD();
                listShotsPageBD.page = page;
                listShotsPageBD.json =  gson.toJson(listShotsPage);
                listShotsPageBD.save();
            }

            @Override
            public void failure(RetrofitError error) {
                ListShotsPageBD listShotsPageBD = ListShotsPageBD.getByPage(page);
                if (listShotsPageBD != null) {
                    Gson gson = new Gson();
                    ListShotsPage listShotsPage = gson.fromJson(listShotsPageBD.json, ListShotsPage.class);
                    showShots(listShotsPage.getShots());
                } else {
                    if (page == 1) {
                        showMessage(true, connectivityErrorListShots);
                    }
                }
                showProgress(false);
                stopRefresh();
            }
        });
    }

    @UiThread
    void stopRefresh() {
        multiSwipeRefreshLayout.setRefreshing(false);
    }

    @UiThread
    void showShots(List<Shot> shots) {
        for (Shot shot : shots) {
            this.shots.add(shot);
        }
        adapter.notifyDataSetChanged();
        showProgress(false);
        showMessage(false, "");
    }

    @UiThread
    void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @UiThread
    void showMessage(boolean show, String message) {
        textViewMessage.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        textViewMessage.setText(message);
    }
}