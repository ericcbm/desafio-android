package com.ericcbm.desafiodribbble;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ericmesquita on 06/06/15.
 */
public interface DribbbleRestClient {
    @GET("/shots/popular")
    void getListShotsPage(@Query("page") int page, Callback<ListShotsPage> cb);

    @GET("/shots/{id}")
    void getShot(@Path("id") long id, Callback<ListShotsPage> cb);
}
