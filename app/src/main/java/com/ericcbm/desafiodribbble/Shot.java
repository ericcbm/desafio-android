package com.ericcbm.desafiodribbble;

import org.jsoup.Jsoup;

/**
 * Created by ericmesquita on 06/06/15.
 */
public class Shot {

    private Long id;
    private String title;
    private String description;
    private Integer height;
    private Integer width;
    private Long likes_count;
    private Integer comments_count;
    private Integer rebounds_count;
    private String url;
    private String short_url;
    private Long views_count;
    private Long rebound_source_id;
    private String image_url;
    private String image_teaser_url;
    private String image_400_url;
    private Player player;
    private String created_at;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description != null ? Jsoup.parse(description).text() : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Long getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(Long likes_count) {
        this.likes_count = likes_count;
    }

    public Integer getComments_count() {
        return comments_count;
    }

    public void setComments_count(Integer comments_count) {
        this.comments_count = comments_count;
    }

    public Integer getRebounds_count() {
        return rebounds_count;
    }

    public void setRebounds_count(Integer rebounds_count) {
        this.rebounds_count = rebounds_count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShort_url() {
        return short_url;
    }

    public void setShort_url(String short_url) {
        this.short_url = short_url;
    }

    public Long getViews_count() {
        return views_count;
    }

    public void setViews_count(Long views_count) {
        this.views_count = views_count;
    }

    public Long getRebound_source_id() {
        return rebound_source_id;
    }

    public void setRebound_source_id(Long rebound_source_id) {
        this.rebound_source_id = rebound_source_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getImage_teaser_url() {
        return image_teaser_url;
    }

    public void setImage_teaser_url(String image_teaser_url) {
        this.image_teaser_url = image_teaser_url;
    }

    public String getImage_400_url() {
        return image_400_url;
    }

    public void setImage_400_url(String image_400_url) {
        this.image_400_url = image_400_url;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public static class Player {
        private Long id;
        private String name;
        private String location;
        private Long followers_count;
        private Integer draftees_count;
        private Long likes_count;
        private Long likes_recieved_count;
        private Integer comments_count;
        private Long comments_recieved_count;
        private Integer rebounds_count;
        private Integer rebounds_recieved_count;
        private String url;
        private String avatar_url;
        private String username;
        private String twitter_screen_name;
        private String website_url;
        private Long drafted_by_player_id;
        private Long shots_count;
        private Long following_count;
        private String created_at;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Long getFollowers_count() {
            return followers_count;
        }

        public void setFollowers_count(Long followers_count) {
            this.followers_count = followers_count;
        }

        public Integer getDraftees_count() {
            return draftees_count;
        }

        public void setDraftees_count(Integer draftees_count) {
            this.draftees_count = draftees_count;
        }

        public Long getLikes_count() {
            return likes_count;
        }

        public void setLikes_count(Long likes_count) {
            this.likes_count = likes_count;
        }

        public Long getLikes_recieved_count() {
            return likes_recieved_count;
        }

        public void setLikes_recieved_count(Long likes_recieved_count) {
            this.likes_recieved_count = likes_recieved_count;
        }

        public Integer getComments_count() {
            return comments_count;
        }

        public void setComments_count(Integer comments_count) {
            this.comments_count = comments_count;
        }

        public Long getComments_recieved_count() {
            return comments_recieved_count;
        }

        public void setComments_recieved_count(Long comments_recieved_count) {
            this.comments_recieved_count = comments_recieved_count;
        }

        public Integer getRebounds_count() {
            return rebounds_count;
        }

        public void setRebounds_count(Integer rebounds_count) {
            this.rebounds_count = rebounds_count;
        }

        public Integer getRebounds_recieved_count() {
            return rebounds_recieved_count;
        }

        public void setRebounds_recieved_count(Integer rebounds_recieved_count) {
            this.rebounds_recieved_count = rebounds_recieved_count;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getTwitter_screen_name() {
            return twitter_screen_name;
        }

        public void setTwitter_screen_name(String twitter_screen_name) {
            this.twitter_screen_name = twitter_screen_name;
        }

        public String getWebsite_url() {
            return website_url;
        }

        public void setWebsite_url(String website_url) {
            this.website_url = website_url;
        }

        public Long getDrafted_by_player_id() {
            return drafted_by_player_id;
        }

        public void setDrafted_by_player_id(Long drafted_by_player_id) {
            this.drafted_by_player_id = drafted_by_player_id;
        }

        public Long getShots_count() {
            return shots_count;
        }

        public void setShots_count(Long shots_count) {
            this.shots_count = shots_count;
        }

        public Long getFollowing_count() {
            return following_count;
        }

        public void setFollowing_count(Long following_count) {
            this.following_count = following_count;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
