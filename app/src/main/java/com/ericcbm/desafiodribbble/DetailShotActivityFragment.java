package com.ericcbm.desafiodribbble;

import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * A placeholder fragment containing a simple view.
 */
@EFragment(R.layout.fragment_detail_shot)
public class DetailShotActivityFragment extends Fragment {

    @ViewById
    TextView textViewShotTitle, textViewViewsCount, textViewPlayerName, textViewDescription;
    @ViewById
    ImageView imageViewShot, imageViewPlayer;


    @AfterViews
    void calledAfterViewInjection() {
        String shotJson = getActivity().getIntent().getStringExtra("EXTRA_SHOT");
        Gson gson = new Gson();
        Shot shot = gson.fromJson(shotJson, Shot.class);
        getActivity().setTitle(shot.getTitle());
        textViewShotTitle.setText(shot.getTitle());
        Picasso.with(getActivity()).load(shot.getImage_url()).fit().centerCrop().into(imageViewShot);
        textViewViewsCount.setText(String.valueOf(shot.getViews_count()));
        Picasso.with(getActivity()).load(shot.getPlayer().getAvatar_url()).fit().centerCrop().placeholder(R.mipmap.profile_placeholder).transform(new CircleTransform()).into(imageViewPlayer);
        textViewPlayerName.setText(shot.getPlayer().getName());
        textViewDescription.setText(shot.getDescription());
    }
}
