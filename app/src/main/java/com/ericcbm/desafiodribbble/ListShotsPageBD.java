package com.ericcbm.desafiodribbble;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by ericmesquita on 6/12/15.
 */
@Table(name = "listshotpage")
public class ListShotsPageBD extends Model {
    @Column(name = "page")
    public int page;
    @Column(name = "json")
    public String json;

    public static ListShotsPageBD getByPage(int page) {
        return new Select()
                .from(ListShotsPageBD.class)
                .where("page = ?", page)
                .executeSingle();
    }
}
