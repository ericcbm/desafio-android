package com.ericcbm.desafiodribbble;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

@EActivity(R.layout.activity_detail_shot)
@OptionsMenu({R.menu.menu_detail_shot})
public class DetailShotActivity extends AppCompatActivity {

    @Extra("EXTRA_SHOT")
    String shotJson;

    private Shot shot;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        initFacebook();
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @AfterExtras
    void calledAfterExtras() {
        Gson gson = new Gson();
        shot = gson.fromJson(shotJson, Shot.class);
    }

    @OptionsItem(R.id.action_facebok)
    void onShareFacebookClick() {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(createFacebookShareContent());
        }
    }

    @OptionsItem(R.id.action_twitter)
    void onShareTwitterClick() {
        final String BASE_URL =
                "https://twitter.com/intent/tweet?";
        final String TEXT_PARAM = "text";
        final String URL_PARAM = "url";
        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendQueryParameter(TEXT_PARAM, shot.getTitle())
                .appendQueryParameter(URL_PARAM, shot.getUrl())
                .build();
        Intent intent = new Intent(Intent.ACTION_VIEW, builtUri);
        try {
            getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent.setPackage("com.twitter.android");
            startActivity(intent);
        } catch (Exception e) {
            startActivity(intent);
        }
    }

    @OptionsItem(R.id.action_google_plus)
    void onShareGooglePlusClick() {
        Intent intent = new PlusShare.Builder(getApplicationContext())
                .setType("text/plain")
                .setText(shot.getTitle())
                .setContentUrl(Uri.parse(shot.getUrl()))
                .getIntent();
        startActivity(intent);
    }

    @OptionsItem(R.id.action_whatsapp)
    void onShareWhatsAppClick() {
        try {
            getPackageManager().getPackageInfo("com.whatsapp", 0);
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage("com.whatsapp");
            intent.putExtra(Intent.EXTRA_TEXT, shot.getTitle() + " " + shot.getUrl());

            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.action_whatsapp_error), Toast.LENGTH_SHORT).show();
        }
    }

    @OptionsItem(android.R.id.home)
    void onHomeClick() {
        onBackPressed();
    }

    private void initFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                //Toast.makeText(getActivity(), "Compartilhado com sucesso!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                //Toast.makeText(getActivity(), "Cancelado com sucesso!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException e) {
                //e.printStackTrace();
                //Log.e(PlayerService.LOG_TAG, "ERRO Facebook: " + e.getMessage());
                //Toast.makeText(getActivity(), "Erro. Tente novamente.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private ShareContent createFacebookShareContent() {
        return new ShareLinkContent.Builder()
                .setContentTitle(shot.getTitle())
                .setContentDescription(shot.getDescription())
                .setContentUrl(Uri.parse(shot.getUrl()))
                .setImageUrl(Uri.parse(shot.getImage_url()))
                .build();
    }
}
