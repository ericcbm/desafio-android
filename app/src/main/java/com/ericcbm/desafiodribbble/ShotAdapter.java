package com.ericcbm.desafiodribbble;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ericmesquita on 06/06/15.
 */
public class ShotAdapter extends RecyclerView.Adapter<ShotAdapter.ViewHolder> {

    private ArrayList<Shot> shots;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textViewShotTitle;
        public ImageView imageViewShot;
        public TextView textViewViewsCount;

        public ViewHolder(View v) {
            super(v);
            textViewShotTitle = (TextView) v.findViewById(R.id.textViewShotTitle);
            imageViewShot = (ImageView) v.findViewById(R.id.imageViewShot);
            textViewViewsCount = (TextView) v.findViewById(R.id.textViewViewsCount);
        }
    }

    public ShotAdapter(Context context, ArrayList<Shot> myDataset) {
        this.context = context;
        shots = myDataset;
    }

    @Override
    public ShotAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_shot, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Shot shot = shots.get(position);
        holder.textViewShotTitle.setText(shot.getTitle());
        Picasso.with(context).load(shot.getImage_url()).fit().centerCrop().into(holder.imageViewShot);
        holder.textViewViewsCount.setText(String.valueOf(shot.getViews_count()));
    }

    @Override
    public int getItemCount() {
        return shots.size();
    }
}